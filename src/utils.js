/* global mw */

const archiveNameRegEx = /\| *archive *= *(.*%\(counter\)d.*) *(-->)?/

const getArchiveName = function (config) {
  let archiveName = archiveNameRegEx.exec(content)

  var rootBase = mw.config.get('wgPageName')
    .replace(/\/.*/g, '')// Chop off the subpages
    .replace(/_/g, ' ')// Replace underscores with spaces
  if (archiveName === null || archiveName === undefined) {
    archiveName = rootBase + '/Archive ' + counter
    errorLog.errorCount++
    errorLog.archiveName = archiveName
  } else {
    archiveName = archiveName[ 1 ]
      .replace(/\| *archive *= */, '')
      .replace(/%\(year\)d/g, thisYear)
      .replace(/%\(month\)d/g, thisMonthNum)
      .replace(/%\(monthname\)s/g, thisMonthFullName)
      .replace(/%\(monthnameshort\)s/g, thisMonthShortName)
      .replace(/%\(counter\)d/g, archiveNum)
    var archiveBase = archiveName
      .replace(/\/.*/, '')// Chop off the subpages
      .replace(/_/g, ' ')// Replace underscores with spaces
    var archiveSub = archiveName
      .replace(/_/g, ' ')// Replace underscores with spaces
      .replace(archiveBase, '')// Chop off the base pagename
    if (archiveBase !== rootBase) {
      errorLog.errorCount++
      errorLog.archiveName = 'Archive name mismatch:<br /><br />Found: ' + archiveName
      errorLog.archiveName += '<br />Expected: ' + rootBase.replace('_', ' ') + archiveSub + '<br /><br />'
    }
  }
}

module.exports = {
  getArchiveName: getArchiveName
}
