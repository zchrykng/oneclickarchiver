/* global CeL */

const fs = require('fs')

fs.readFile('dist/OneClickArchiver.js', 'utf-8', function (err, content) {
  if (err) {
    console.log(err)
    return
  }
  require('cejs')
  CeL.run('application.net.wiki')
  CeL.wiki.set_language('en')

  const wiki = CeL.wiki.login(process.env.USUNAME, process.env.USUPASS, 'en')

  wiki.page('User:Zchrykng/Scripts/OneClickArchiver.js').edit(content, {
    summary: 'update script with latest commit'
  })
})
